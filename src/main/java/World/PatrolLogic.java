package world;

import java.util.Random;


public class PatrolLogic {
    private final Position home;
    private final int patrolDistance;


    public PatrolLogic() {
        home = null;
        patrolDistance = Integer.MAX_VALUE;
    }

    public PatrolLogic(Position home, int patrolDistance) {
        this.home = home;
        this.patrolDistance = patrolDistance;
    }


    public Position nextPosition(MondeDuJeu w, Position current) {

        // Take a random step
        final Map m = w.getMap();
        Random rnd = w.getRandom();
        int row = rnd.nextInt(3) - 1;
        int col = rnd.nextInt(3) - 1;
        Position newpos = new Position(current.row() + row,
                current.col() + col);
        if (!m.withinMap(newpos) || !w.isOpen(newpos)) {
            return current;
        }

        // If the new position takes the entity outside of its patrol radius
        // don't take the step.
        if (home != null && newpos.distanceTo(home) >= patrolDistance) {
            return current;
        }

        return newpos;
    }
}
