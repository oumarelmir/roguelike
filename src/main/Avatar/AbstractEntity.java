package Avatar;

import world.MondeDuJeu;
import world.Position;
import world.Tile;

public abstract class AbstractEntity {
    private final String name;
    private final Tile tile;
    private Position pos;

    public AbstractEntity(String name, Tile tile, Position pos) {
        this.name = name;
        this.pos = pos;
        this.tile = tile;
    }

    public String getName() {return name;  }

    public void setPosition(Position pos) {  this.pos = pos; }
    public Position getPosition() {    return pos; }
    public Tile getTile() {  return tile; }

    abstract public void tick(MondeDuJeu w);
}
