package world;

public class Combattre {

      public static void combat(MondeDuJeu w, AttackableEntity attacker, AttackableEntity defender) {
        // Determine the maximum damage the attacker can do to the
        // defender
        int dmg = Math.max(0, attacker.getDamagePerTurn() - defender.getArmour());
        
        
        dmg = Math.min(dmg, defender.getHealth());

        // Randomise between 0 and max damage
        dmg = w.getRandom().nextInt(dmg + 1);

        // Apply the damage
        defender.damage(dmg);

        // Report the event via messages if the player is involved
        Messages msg = w.getMessages();
       
        if (attacker == w.getJoueur()) {
            if (dmg > 0) {
                msg.add("Vous frappez un" + defender.getName() + " pour " + dmg + " dégats");
            } else {
                msg.add("Vous balancez à un " + defender.getName());
            }
            if (defender.getHealth() == 0) {
            	
            	// References.PLAYER_STARTING_MAX_HEALTH = References.PLAYER_STARTING_MAX_HEALTH + 1 ;  
                msg.add("Vous tuez un " + defender.getName());
            }
        } else if (defender == w.getJoueur()) {
            if (dmg > 0) {
                msg.add("un " + attacker.getName() + " vous a frappé pour " + dmg + " dégats");
            } else {
                msg.add("Un " + attacker.getName() + " vous a frappé");
            }
        } 
    }
}
