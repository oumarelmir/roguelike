package screen;

import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;

import world.MondeDuJeu;
import world.Position;
import world.Tile;


public class SidebarPane extends Pane {

    public SidebarPane(Screen s, int height, int width, Position corner) {
        super(s, height, width, corner);
    }

    @Override
    void draw(MondeDuJeu w) {

        int row = corner().row();
        int col = corner().col();

       
        String health = "Points de vie: " + w.getJoueur().getHealth()
                + "/" + w.getJoueur().getMaxHealth();

        screen().putString(col, row, health, Terminal.Color.WHITE,
                Terminal.Color.BLACK);
      

      
        row += 4;
        screen().putString(col, row, "  Tiles : ", Terminal.Color.MAGENTA,
                Terminal.Color.BLACK);
        
        row++;
        for (Tile t : Tile.values()) {
            if (t.getLabel().equals("")) continue;

            screen().putString(col, row, t.getGlyph() + " : " + t.getLabel(),
                    t.getColour(), Terminal.Color.BLACK);
            row++;
        }

       
        row += 2;
        screen().putString(col, row, "  Actions :", Terminal.Color.MAGENTA,
                Terminal.Color.BLACK);
        row++;
        screen().putString(col, row, "\u2190 : Left", Terminal.Color.WHITE,
                Terminal.Color.BLACK);
        row++;
        screen().putString(col, row, "\u2192 : Right", Terminal.Color.WHITE,
                Terminal.Color.BLACK);
        row++;
        screen().putString(col, row, "\u2191 : Up", Terminal.Color.WHITE,
                Terminal.Color.BLACK);  
        row++;
        screen().putString(col, row, "\u2193 : Down", Terminal.Color.WHITE,
                Terminal.Color.BLACK);
        row++;
        screen().putString(col, row, "q : Quit", Terminal.Color.WHITE,
                Terminal.Color.BLACK);
    }

}
