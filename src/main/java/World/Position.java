package world;


public class Position {
    private final int row;
    private final int col;

    public Position(int _row, int _col) {
        row = _row;
        col = _col;
    }


    public int row() {return row; }
    public int col() { return col; }

    public int distanceTo(Position other) {
        if (this.equals(other)) return 0;

        int diffrow = Math.abs(row() - other.row());
        int diffcol = Math.abs(col() - other.col());
        return (int) Math.floor(Math.sqrt((diffrow * diffrow) + (diffcol * diffcol)));
    }

     @Override
    public boolean equals(Object o) {
        if (!(o instanceof Position)) return false;
        Position other = (Position) o;
        return row == other.row && col == other.col;
    }

    @Override
    public int hashCode() {
        return row + (31 * col);
    }

    @Override
    public String toString() {  return "(" + row() + ", " + col() + ")"; }
}
