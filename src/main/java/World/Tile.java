package world;

import com.googlecode.lanterna.terminal.Terminal;


public enum Tile {
    COULOIR('#', "Wall", Terminal.Color.WHITE),
    SOL('.', "Sol", Terminal.Color.GREEN),
    JOUEUR('@', "Player", Terminal.Color.CYAN, true),
    MONSTER('X', "Monster", Terminal.Color.RED),
    AGENT('A', "Agent", Terminal.Color.BLUE),
	TRESOR('*', " Tresors", Terminal.Color.YELLOW);

    private final char glyph;
    private final String label;
    private final Terminal.Color colour;
    private final boolean bold;

    Tile(char glyph, String label, Terminal.Color colour, boolean bold) {
        this.glyph = glyph;
        this.label = label;
        this.colour = colour;
        this.bold = bold;
    }

    Tile(char glyph, String label, Terminal.Color colour) {
        this(glyph, label, colour, false);
    }

    public char getGlyph() {
        return glyph;
    }

    public String getLabel() {
        return label;
    }

    public Terminal.Color getColour() {
        return colour;
    }

    public boolean getBold() {
        return bold;
    }
}
