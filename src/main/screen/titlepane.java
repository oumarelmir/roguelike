package screen;

import roguelike.References;
import world.MondeDuJeu;
import world.Position;

import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;


public class TitlePane extends Pane {

    public TitlePane(Screen s, int height, int width, Position corner) {
        super(s, height, width, corner);
    }

    @Override
    public void draw(MondeDuJeu w) {
        int row = corner().row();
        int col = corner().col() + (width() - References.TITLE.length()) / 2;

        screen().putString(col, row, References.TITLE,
                Terminal.Color.BLACK, Terminal.Color.WHITE);
    }

}
