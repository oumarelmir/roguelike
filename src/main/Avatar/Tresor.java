package Avatar;


import roguelike.References;
import world.Position;
import world.Tile;

public class Tresor extends AbstractPatrollingNPC {

    public Tresor(Position pos) {
        super("Tresor", pos, Tile.TRESOR, References.Tresor_MAX_HEALTH, , , Faction.FRIENDLY);
    }
}

