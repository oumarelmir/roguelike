package world;

import java.util.Random;

import Avatar.Agent;
import Avatar.Monstres;
import Avatar.Tresor;
import roguelike.References;


public class MondeGeneration {


    public static MondeDuJeu generate(Random rnd) {
        Map m = MapGenerate.generation(References.MAP_HEIGHT, References.MAP_WIDTH, rnd);
        MondeDuJeu w = new MondeDuJeu(m, rnd);

        // Find an open map position for the player to start
        Position pos = findOpenPosition(w);
        w.getJoueur().setPosition(pos);

        spawnMonstres(w, References.NPE_MONSTRE_COUNT);
        spawnAgents(w, References.NPE_AGENT_COUNT);
        spawnTresor(w, References.NPE_Tresor_COUNT);
        return w;
    }


    private static void spawnMonstres(MondeDuJeu w, int n) {
        for (int i = 0; i < n; ++i) {
            w.addEntity(new Monstres(findOpenPosition(w)));
        }
    }

    private static void spawnAgents(MondeDuJeu w, int n) {
        for (int i = 0; i < n; ++i) {
            w.addEntity(new Agent(findOpenPosition(w)));
        }
    }
    
    private static void spawnTresor(MondeDuJeu w, int n) {
        for (int i = 0; i < n; ++i) {
            w.addEntity(new Tresor(findOpenPosition(w)));
        }
    }
    
    private static Position findOpenPosition(MondeDuJeu w) {
        Map m = w.getMap();
        Random rnd = w.getRandom();
        Position pos;
        while (true) {
            int row = rnd.nextInt(m.nrows());
            int col = rnd.nextInt(m.ncols());
            pos = new Position(row, col);
            if (w.isOpen(pos)) {
                break;
            }
        }

        return pos;
    }
    


}
