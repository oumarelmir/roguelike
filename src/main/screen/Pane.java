package screen;


import com.googlecode.lanterna.screen.Screen;

import world.MondeDuJeu;
import world.Position;


public abstract class Pane {
    private final Screen screen;
    private final int height;
    private final int width;
    private final Position corner;

    public Pane(Screen s, int height, int width, Position corner) {
        this.screen = s;
        this.height = height;
        this.width = width;
        this.corner = corner;
    }

    abstract void draw(MondeDuJeu w);
    protected Screen screen() {  return screen;  }

    int height() { return height;}
    int width() { return width;  }
    Position corner() {return corner; }
}
