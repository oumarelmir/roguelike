package screen;

import roguelike.References;
import world.MondeDuJeu;
import world.Position;

import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalSize;


public class Interface implements Terminal.ResizeListener {
    private final Screen screen;

    private TitlePane titlePane;
    private MapPane mapPane;
    private TextPane textPane;
    private SidebarPane sidebarPane;

    public Interface(Screen s) {
        this.screen = s;
        s.clear();
        s.refresh();
        initPanes();

        s.getTerminal().addResizeListener(this);
    }

    public void draw(MondeDuJeu w) {
        screen.clear();
        titlePane.draw(w);
        mapPane.draw(w);
        textPane.draw(w);
        sidebarPane.draw(w);

        screen.refresh();
    }

    private void initPanes() {
        final int ncols = screen.getTerminalSize().getColumns();
        final int nrows = screen.getTerminalSize().getRows();

        // Setup Title Pane
        titlePane = new TitlePane(screen, nrows, ncols
                - References.PADDING_LEFT - References.PADDING_RIGHT,
                new Position(References.PADDING_TOP, References.PADDING_LEFT));

        // Setup Map Pane
        int mapHeight = nrows - References.PADDING_TOP - References.TITLE_HEIGHT
                - References.PADDING_TITLE_MAP - References.PADDING_MAP_TEXT
                - References.MESSAGES_HEIGHT - References.PADDING_BOTTOM;

        int mapWidth = ncols - References.PADDING_LEFT
                - References.PADDING_MAP_SIDEBAR - References.SIDEBAR_WIDTH
                - References.PADDING_RIGHT;

        Position mapTlc = new Position(References.PADDING_TOP + References.TITLE_HEIGHT
                + References.PADDING_TITLE_MAP - 1,
                References.PADDING_LEFT);

        mapPane = new MapPane(screen, mapHeight, mapWidth, mapTlc);


        // Setup Text Pane
        Position textAreaTlc = new Position(
                mapTlc.row() + mapPane.height() + References.PADDING_MAP_TEXT,
                References.PADDING_LEFT);

        textPane = new TextPane(screen, References.MESSAGES_HEIGHT,
                mapPane.width(), textAreaTlc);

        // Setup Sidebar Pane        
        Position sidebarTlc = new Position(
                References.PADDING_TOP + References.TITLE_HEIGHT + References.PADDING_TITLE_MAP,
                ncols - 1 - References.SIDEBAR_WIDTH);

        sidebarPane = new SidebarPane(screen, mapHeight, References.SIDEBAR_WIDTH,
                sidebarTlc);
    }

    public void onResized(TerminalSize termSize) {
        // TODO: Handle this correctly
        screen.refresh();
    }
}
