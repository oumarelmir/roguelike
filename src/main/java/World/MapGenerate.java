package world;

import java.util.Random;


 
public class MapGenerate {

 
    public static Map generation(int nrows, int ncols, Random rnd) {
        Map m = new Map(nrows, ncols);
        randomise(m, rnd);

        final int iterations = 5;
        for (int i = 0; i < iterations; i++) {
            if (i < iterations - 1) {
                smooth(m, true);
            } else {
                smooth(m, false);
            }
        }

        return m;
    }

    private static void randomise(Map m, Random rnd) {
        final double PROBABILITY_WALL = 0.44;
        Tile wall = Tile.COULOIR;

        for (int row = 0; row < m.nrows(); row++) {
            for (int col = 0; col < m.ncols(); col++) {
                if (rnd.nextDouble() < PROBABILITY_WALL) {
                    m.setTile(row, col, wall);
                }
            }
        }
    }
    
    private static void smooth(Map m, boolean fillEmpty) {
        Tile wall = Tile.COULOIR;
        Tile open = Tile.SOL;

        final int nrows = m.nrows();
        final int ncols = m.ncols();

        for (int row = 0; row < nrows; row++) {
            for (int col = 0; col < ncols; col++) {
                int wallcount = 0;

                // Compter le nombre de carreaux muraux entourant la tuile à
                // position row / col. Toute tuile en dehors des limites de la carte
                // sont comptés comme des murs. Cela garantit plus de «mur» sur la carte
                // bords.
                for (int i = row - 1; i <= row + 1; i++) {
                    for (int j = col - 1; j <= col + 1; j++) {
                        if (i < 0 || j < 0 || i >= nrows || j >= ncols) {
                            wallcount++;
                        } else {
                            if (m.getTile(i, j) == Tile.COULOIR) {
                                wallcount++;
                            }
                        }
                    }
                }

                // Apply smoothing using cellular automata approach.
                if (wallcount > 4 || (fillEmpty && wallcount == 0)) {
                    m.setTile(row, col, wall);
                } else {
                    m.setTile(row, col, open);
                }
            }
        }
    }
}