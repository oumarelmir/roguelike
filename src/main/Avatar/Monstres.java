package Avatar;

import roguelike.References;
import world.Position;
import world.Tile;


public class Monstres extends AbstractPatrollingNPC {

    public Monstres(Position pos) {
        super("Monstre", pos, Tile.MONSTER, References.MONSTRE_MAX_HEALTH,
                References.MONSTRE_ARMOUR, References.MONSTRE_MAX_DAMAGE, Faction.MONSTER);
    }
}
