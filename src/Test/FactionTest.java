package roguelikeFaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Avatar.Faction;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FactionTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public final void testIsHostile() {
        assertTrue(Faction.FRIENDLY.isHostile(Faction.MONSTER));
        assertTrue(Faction.MONSTER.isHostile(Faction.FRIENDLY));
        
        assertFalse(Faction.FRIENDLY.isHostile(Faction.FRIENDLY));
        assertFalse(Faction.MONSTER.isHostile(Faction.MONSTER));
    }
}
