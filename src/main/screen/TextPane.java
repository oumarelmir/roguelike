package screen;

import java.util.ArrayList;
import java.util.List;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;

import world.MondeDuJeu;
import world.Position;


public class TextPane extends Pane {

    public TextPane(Screen s, int height, int width, Position corner) {
        super(s, height, width, corner);
    }

    @Override
    void draw(MondeDuJeu w) {
        List<String> text = wrapLines(w.getMessages().getTextList(),
                width(), height());

        int row = corner().row();
        for (String s : text) {
            assert (s.length() < width());

            screen().putString(corner().col(), row, s,
                    Terminal.Color.WHITE, Terminal.Color.BLACK);
            row++;
        }
    }

    static private List<String> wrapLines(List<String> lst,
                                          int maxLen, int maxHeight) {
        // Pre-conditions
        assert lst != null;
        assert maxLen > 0;
        assert maxHeight > 0;

        List<String> v = new ArrayList<>();
        for (String s : lst) {
            if (s.length() < maxLen) {
                v.add(s);
            } else {
                v.addAll(v.size(), wrapString(s, maxLen));
            }
        }

        while (v.size() > maxHeight) v.remove(0);

        // Post-conditions
        assert v.size() <= maxHeight;
        for (String s : v) assert s.length() <= maxLen;

        return v;
    }

 
    static private List<String> wrapString(String s, int maxLen) {
        s = s.trim();
        List<String> v = new ArrayList<>();
        int beginIndex = 0;
        while (beginIndex < s.length()) {
            int subStrLen = Math.min(maxLen, s.length() - beginIndex);
            int endIndex = subStrLen + beginIndex;

            // TODO: Fix this hack - This is simply trying to find the best position
            // to break the string by moving backwards and looking for a space. Some
            // strings may break it.
            if (subStrLen == maxLen) {
                int lastSpace = s.lastIndexOf(' ', endIndex);
                if (lastSpace > (beginIndex + 1)) endIndex = lastSpace;
            }

            v.add(s.substring(beginIndex, endIndex).trim());
            beginIndex = endIndex;
        }
        return v;
    }
}
