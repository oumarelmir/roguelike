package roguelike;

import world.JoueurActions;
import world.MondeDuJeu;
import world.MondeGeneration;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.swing.SwingTerminal;

import screen.Interface;


public class App {
    private final Map<Character, JoueurActions> actionKeys = new HashMap<>();

    public App() {
    	create();
    }

    private void create() {
    	actionKeys.put('u', JoueurActions.MOVE_UP);
    	actionKeys.put('d', JoueurActions.MOVE_DOWN);
    	actionKeys.put('l', JoueurActions.MOVE_LEFT);
    	actionKeys.put('r', JoueurActions.MOVE_RIGHT);
    }

    
    private static void displayTitle(Screen s) {
        final String msg = References.TITLE + ": Press any key to start" ;
      
        int ypos = s.getTerminalSize().getRows() / 2;
        int xpos = s.getTerminalSize().getColumns() / 2;

        s.putString(xpos, ypos, msg, Terminal.Color.BLUE,Terminal.Color.BLACK);
      
        s.refresh();

        // Wait for any key press
        getKeyBlocking(s);
    }
    
    private static Key getKeyBlocking(Screen s) {
        Key k = null;
        while (k == null) {
            k = s.getTerminal().readInput();
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                return null;
            }
        }
        return k;
    }

    private static MondeDuJeu createWorld() {
        return MondeGeneration.generate(new Random(System.currentTimeMillis()));
    }

   
    private void gameLoop(MondeDuJeu w, Screen s) {
        Interface ui = new Interface(s);
        ui.draw(w);

        while (true) {
            Key k = getKeyBlocking(s);
            Character c = Character.toLowerCase(k.getCharacter());
            if (c == 'q')
                break;

            JoueurActions act = actionKeys.get(c);
            if (act == null) {
                continue;
            }
            w.tick(act);
            ui.draw(w);
        }
    }

  

 

    private void jouer() {
       
        Screen s = TerminalFacade.createScreen(new SwingTerminal(
        		References.SCREEN_NCOLS, References.SCREEN_NROWS));
        s.startScreen();
        displayTitle(s);

        MondeDuJeu w = createWorld();
        w.getMessages().add("Les Messages: ");

   
        gameLoop(w, s);
        s.stopScreen();
    }

    public static void main(String[] args) {
    	App app = new App();
        app.jouer();
    }
}
