package roguelikeWorld;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Avatar.AbstractEntity;
import world.IAttackable;
import world.JoueurActions;
import world.Map;
import world.MondeDuJeu;
import world.MondeGeneration;
import world.Position;
import world.Tile;

import java.util.Random;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class WorldTest {
    /** Class under test */
    private MondeDuJeu instance;

    @Before
    public void setUp() throws Exception {
        instance = MondeGeneration.generate(new Random());
    }

    @After
    public void tearDown() throws Exception {
        instance = null;
    }

    /**
     * This is the main world test, ensuring the world maintains basic
     * consistency for 1000 ticks.
     */
    @Test
    public final void testOneThousandTicks() {
        final int N_ITERATIONS = 1000;
        for (int i = 0; i < N_ITERATIONS; ++i) {
            instance.tick(JoueurActions.MOVE_UP);
            validateWorld(instance);
        }
    }

    /**
     * Performs basic consistency checks on the world
     */
    private void validateWorld(MondeDuJeu w) {
        assertNotNull(w.getJoueur());
        assertNotNull(w.getEntities());
        assertNotNull(w.getMap());
        assertNotNull(w.getRandom());
        
        // Ensure all entities are within map bounds
        for (AbstractEntity e : w.getEntities()) {
            assertWithinMapBounds(w.getMap(), e);
            assertTrue(w.getMap().getTile(e.getPosition()) == Tile.SOL);
        }
        
        // Ensure all attackable entities have health 0 (died this turn,
        // or greater
        for (AbstractEntity e : w.getEntities()) {
            if (e instanceof IAttackable) {
                IAttackable ia = (IAttackable) e;
                assertTrue(ia.getHealth() >= 0);
            }
        }
    }
    
    /**
     * Confirms the entity is within the map bounds.
     */
    private void assertWithinMapBounds(Map map, AbstractEntity e) {
        assertNotNull(map);
        assertNotNull(e);
        
        Position pos = e.getPosition();
        assertTrue(pos.row() >= 0);
        assertTrue(pos.col() >= 0);
        assertTrue(pos.row() < map.nrows());
        assertTrue(pos.col() < map.ncols());
    }
}
