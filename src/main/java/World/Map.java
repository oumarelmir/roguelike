package world;

import java.util.ArrayList;
import java.util.List;


public class Map {
    private final List<Tile> data;
    private final int nrows;
    private final int ncols;

    public Map(int nrows, int ncols) {
        // Pre-conditions
        assert (nrows > 0);
        assert (ncols > 0);

        this.nrows = nrows;
        this.ncols = ncols;

        // Initialise data vector
        data = new ArrayList<>(nrows * ncols);
        for (int i = 0; i < nrows * ncols; ++i) {
            data.add(Tile.SOL);
        }
    }

   
    public int nrows() {   return nrows;  }
    public int ncols() {      return ncols;  }

   
    public Tile getTile(int row, int col) {
        checkPosition(row, col);
        return data.get((row * ncols) + col);
    }

    public Tile getTile(Position p) {
        return getTile(p.row(), p.col());
    }

  
    public void setTile(int row, int col, Tile t) {
        checkPosition(row, col);
        data.set(((row * ncols) + col), t);
    }


    public void setTile(Position p, Tile t) {
        setTile(p.row(), p.col(), t);
    }

 
    public Boolean withinMap(Position pos) {
        return (pos.row() >= 0 && pos.row() < nrows
                && pos.col() >= 0 && pos.col() < ncols);
    }


    private void checkPosition(int row, int col) {
        if (row > nrows - 1) {
            throw new IndexOutOfBoundsException("Row: " + row
                    + " is out of bounds");
        }
        if (col > ncols - 1) {
            throw new IndexOutOfBoundsException("Column: " + col
                    + " is out of bounds");
        }
    }
}
