package Avatar;

import roguelike.References;
import world.Position;
import world.Tile;


public class Agent extends AbstractPatrollingNPC {

    public Agent(Position pos) {
        super("Agent", pos, Tile.AGENT, References.AGENT_MAX_HEALTH,
                References.AGENT_ARMOUR, References.AGENT_MAX_DAMAGE, Faction.FRIENDLY);
    }
}
