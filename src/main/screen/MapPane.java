package screen;

import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal;

import Avatar.AbstractEntity;
import world.Joueur;
import world.Map;
import world.MondeDuJeu;
import world.Position;
import world.Tile;


public class MapPane extends Pane {
    private final int firstScreenRow;
    private final int firstScreenCol;

    // Width of the map area (i.e. pane width minus borders)
    private final int mapWidth;

    // Height of the map area (i.e. pane height minus borders)
    private final int mapHeight;

    public MapPane(Screen s, int height, int width, Position corner) {
        super(s, height, width, corner);

        firstScreenRow = corner().row() + 1;
        firstScreenCol = corner().col() + 1;
        mapWidth = width() - 2;
        mapHeight = height() - 2;
    }

    @Override
    void draw(MondeDuJeu w) {
        drawBorders();
        drawPane(w);
    }

    private void drawBorders() {
        StringBuilder hline = new StringBuilder();
        hline.append("~");
        for (int i = 0; i < width() - 2; ++i) {
            hline.append("~");
        }
        hline.append("~");

        final int tlcRow = corner().row();
        final int tlcCol = corner().col();

 
        screen().putString(tlcCol, tlcRow, hline.toString(),
                Terminal.Color.BLUE, Terminal.Color.BLACK);

         screen().putString(tlcCol, tlcRow + height() - 1, hline.toString(),
                Terminal.Color.BLUE, Terminal.Color.BLACK);

        
        for (int row = tlcRow + 1; row < (tlcRow + height() - 1); row++) {
            screen().putString(tlcCol, row, "^", Terminal.Color.GREEN,
                    Terminal.Color.BLACK);
            screen().putString(tlcCol + width() - 1, row, "^",
                    Terminal.Color.GREEN, Terminal.Color.BLACK);
        }
    }

    private void drawPane(MondeDuJeu w) {
        final Map m = w.getMap();
        final Joueur ply = w.getJoueur();

        // Calculate the map position which will align with the top-left-corner
        // of the pane. This depends on the player position, try to keep the
        // player in the centre, however as the player approaches the edge of
        // the map stays stationary and the player moves.
        int mapTlcRow = ply.getPosition().row() - (mapHeight / 2);
        mapTlcRow = Math.max(0, mapTlcRow);
        mapTlcRow = Math.min(m.nrows() - mapHeight, mapTlcRow);

        int mapTlcCol = ply.getPosition().col() - (mapWidth / 2);
        mapTlcCol = Math.max(0, mapTlcCol);
        mapTlcCol = Math.min(m.ncols() - mapWidth, mapTlcCol);

        // Draw
        drawMap(mapTlcRow, mapTlcCol, m);
        drawEntities(mapTlcRow, mapTlcCol, w);
    }

    // Draws the map tiles to the map pane
    private void drawMap(int mapTlcRow, int mapTlcCol, Map m) {
        for (int row = 0; row < mapHeight; row++) {
            for (int col = 0; col < mapWidth; col++) {
                Tile t = m.getTile(mapTlcRow + row, mapTlcCol + col);

                screen().putString(firstScreenCol + col, firstScreenRow + row,
                        Character.toString(t.getGlyph()), t.getColour(),
                        Terminal.Color.BLACK);
            }
        }
    }

    // Draw all non-player entities on to the map pane
    private void drawEntities(int mapTlcRow, int mapTlcCol,
                              MondeDuJeu w) {
        for (AbstractEntity e : w.getEntities()) {
            Position pos = e.getPosition();

            // Does the entity fall within the visible part of the map?
            if (pos.row() >= mapTlcRow && pos.row() < mapTlcRow + mapHeight
                    && pos.col() >= mapTlcCol
                    && pos.col() < mapTlcCol + mapWidth) {
                drawEntity(mapTlcRow, mapTlcCol, e, e == w.getJoueur());
            }
        }
    }

    // Draw a single entity on the map pane
    private void drawEntity(int mapTlcRow, int mapTlcCol, AbstractEntity e,
                            boolean isPlayer) {
        Position pos = e.getPosition();

        // Calculate draw position
        int drawcol = firstScreenCol + (pos.col() - mapTlcCol);
        int drawrow = firstScreenRow + (pos.row() - mapTlcRow);

        Tile t = e.getTile();
        if (t.getBold()) {
            screen().putString(drawcol, drawrow,
                    Character.toString(t.getGlyph()), t.getColour(),
                    Terminal.Color.BLACK, ScreenCharacterStyle.Bold);
        } else {
            screen().putString(drawcol, drawrow,
                    Character.toString(t.getGlyph()), t.getColour(),
                    Terminal.Color.BLACK);
        }

        if (isPlayer) {
            screen().setCursorPosition(drawcol, drawrow);
        }
    }
}
