package Avatar;

import world.AttackableEntity;
import world.Combattre;
import world.MondeDuJeu;
import world.PatrolLogic;
import world.Position;
import world.Tile;


public class AbstractPatrollingNPC extends AttackableEntity {
    private final static int ATTACK_RANGE = 1;
    private final static int PATROL_RANGE = 5; 

    private final PatrolLogic patrol;
    private final int damagePerTurn;

    public AbstractPatrollingNPC(String name,
                                 Position pos,
                                 Tile tile,
                                 int initialHealth,
                                 int initialArmour,
                                 int damagePerTurn,
                                 Faction faction) {
        super(name, tile, pos, initialHealth, initialArmour, faction);
        this.damagePerTurn = damagePerTurn;
        patrol = new PatrolLogic(pos, PATROL_RANGE);
    }

    @Override
    public void tick(MondeDuJeu w) {
        AttackableEntity e = findTarget(w, ATTACK_RANGE);
        if (e != null) {
        	Combattre.combat(w, this, e);
        } else {
            setPosition(patrol.nextPosition(w, getPosition()));
        }
    }

    @Override
    public int getDamagePerTurn() {
        return damagePerTurn;
    }

 
    private AttackableEntity findTarget(MondeDuJeu w, int range) {
        for (AbstractEntity e : w.getEntities()) {
            if (!(e instanceof AttackableEntity)) continue;
            AttackableEntity ae = (AttackableEntity) e;

            if (getPosition().distanceTo(ae.getPosition()) <= range
                    && getFaction().isHostile(ae.getFaction())) {
                return ae;
            }
        }
        return null;
    }
}