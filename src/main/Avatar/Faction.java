package Avatar;

public enum Faction {
	FRIENDLY,
    MONSTER;

    /**
     * @param other the faction to consider for hostility.
     * @return true if this faction "other" is hostile to "this" faction
     */
    public boolean isHostile(Faction other) {
        return other != this;
    }
}