package roguelikePosition;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import world.Position;

import static org.junit.Assert.*;

public class PositionTest {
    private final Position p0 = new Position(0,0);
    private final Position p1 = new Position(0,0);
    private final Position p2 = new Position(1,0);
    private final Position p3 = new Position(0,1);
    private final Position p4 = new Position(1,1);
    private final Position p5 = new Position(1,9);

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public final void testRow() {
        assertEquals(0, p1.row());
        assertEquals(1, p2.row());
    }

    @Test
    public final void testCol() {
        assertEquals(0, p1.col());
        assertEquals(1, p3.col());
    }

    @Test
    public final void testDistanceTo() {
        assertEquals(0, p0.distanceTo(p0));
        assertEquals(0, p0.distanceTo(p1));
        
        assertEquals(1, p1.distanceTo(p2));
        assertEquals(1, p1.distanceTo(p3));
        assertEquals(1, p1.distanceTo(p4));
        
        assertEquals(8, p4.distanceTo(p5));
        assertEquals(9, p0.distanceTo(p5));
    }

    @Test
    public final void testEqualsObject() {
        assertTrue(p0.equals(p0));
        assertTrue(p1.equals(p0));
        assertTrue(p0.equals(p1));
        
        assertFalse(p1.equals(p2));
        assertFalse(p1.equals(p3));
        assertFalse(p1.equals(p4));
    }
}
