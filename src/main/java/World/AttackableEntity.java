package world;

import Avatar.AbstractEntity;
import Avatar.Faction;

public abstract class AttackableEntity extends AbstractEntity implements
        IAttackable {
    private int health;
    private int armour;
    private final Faction faction;

    public AttackableEntity(String name, Tile tile, Position pos,
                            int initialHealth, int initialArmour, Faction faction) {
        super(name, tile, pos);
        health = initialHealth;
        armour = initialArmour;
        this.faction = faction;
    }

    @Override
    public int getHealth() {
        return health;
    }
 
    @Override
    public void damage(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException();
        }
        health = Math.max(0, health - amount);
    }

  
    @Override
    public void heal (int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException();
        }

        if (Integer.MAX_VALUE - health <= amount) {
            health = Integer.MAX_VALUE;
        } else {
            health += amount;
        }
    }

    @Override
    public int getArmour() {
        return armour;
    }

    @Override
    public void setArmour(int armour) {
        this.armour = armour;
    }

    public Faction getFaction() {
        return faction;
    }
}
