package world;

import Avatar.Faction;
import roguelike.References;


public class Joueur extends AttackableEntity {

    private int maxHealth = References.PLAYER_STARTING_MAX_HEALTH;
    private int gold = References.PLAYER_STARTING_GOLD;

  
    public Joueur(Position pos) {
        super("Vous", Tile.JOUEUR, pos, References.PLAYER_STARTING_MAX_HEALTH,
                References.PLAYER_STARTING_ARMOUR_VALUE,
                Faction.FRIENDLY);
    }

    
    public int getMaxHealth() {return maxHealth; }
    public void setMaxHealth(int maxHealth) { this.maxHealth = maxHealth; }
    public int getGold() { return gold;  }
    public void setGold(int gold) {   this.gold = gold;  }

    @Override
    public void tick(MondeDuJeu w) {  }

    public void playerTick(MondeDuJeu w, JoueurActions action) {
        int row = getPosition().row();
        int col = getPosition().col();

        Position newPos;

        switch (action) {
            case MOVE_UP:
                newPos = new Position(row - 1, col);
                break;
            case MOVE_DOWN:
                newPos = new Position(row + 1, col);
                break;
            case MOVE_LEFT:
                newPos = new Position(row, col - 1);
                break;
            case MOVE_RIGHT:
                newPos = new Position(row, col + 1);
                break;
            default:
                w.getMessages().add("Unknown Key");
                return;
        }

        if (w.isOpen(newPos)) {
            setPosition(newPos);
        } else {
            if (w.isAttackable(newPos)) {
                AttackableEntity e = (AttackableEntity) w.getEntityAtPos(newPos);
                Combattre.combat(w, w.getJoueur(), e);
                if (e.getHealth() == 0) {
                    setPosition(newPos);
                }
            } else {
                w.getMessages().add("Ne peut pas y aller");
            }
        }
    }

    @Override
    public int getDamagePerTurn() {
        return References.PLAYER_MAX_DAMAGE;
    }
}
