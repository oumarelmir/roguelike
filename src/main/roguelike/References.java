package roguelike;

public class References {

    public static final String TITLE = "  Rogue like  ";
    
    public static final int MAP_WIDTH = 200;
    public static final int MAP_HEIGHT = 200;

    public static final int SCREEN_NCOLS = 180;
    public static final int SCREEN_NROWS = 40;

    public static final int MESSAGES_HEIGHT = 6;

    public static final int SIDEBAR_WIDTH = 30;
    
    public static final int TITLE_HEIGHT = 1;

    public static final int PADDING_TOP = 1;
    public static final int PADDING_BOTTOM = 1;
    public static final int PADDING_LEFT = 1;
    public static final int PADDING_RIGHT = 1;
    public static final int PADDING_TITLE_MAP = 2;
    public static final int PADDING_MAP_TEXT = 1;
    public static final int PADDING_MAP_SIDEBAR = 1;
    public static final int PADDING_TOP_SIDEBAR = 1;

    public static final int PLAYER_STARTING_MAX_HEALTH = 150;
    public static final int PLAYER_STARTING_ARMOUR_VALUE = 0;
    public static final int PLAYER_STARTING_GOLD = 0;
    public static final int PLAYER_MAX_DAMAGE = 4;

    
    public static final int NPE_MONSTRE_COUNT = 100;
    public static final int MONSTRE_MAX_HEALTH = 10;
    public static final int MONSTRE_ARMOUR = 0;
    public static final int MONSTRE_MAX_DAMAGE = 5;
   
    
    public static final int NPE_Tresor_COUNT = 30;
    public static final int Tresor_MAX_HEALTH = 1;

    
    public static final int NPE_AGENT_COUNT = 50;
    public static final int AGENT_MAX_HEALTH = 10;
    public static final int AGENT_ARMOUR = 1;
    public static final int AGENT_MAX_DAMAGE = 3;
}
